<?php

function qcloud_cos_settings_form($form, &$form_state) {
  $form['required'] = [
    '#type' => 'fieldset',
    '#title' => '腾讯云COS配置',
  ];
  $form['required']['qcloud_cos:appid'] = [
    '#type' => 'textfield',
    '#title' => 'QCloud COS Appid',
    '#default_value' => variable_get('qcloud_cos:appid', ''),
    '#required' => TRUE,
  ];
  $form['required']['qcloud_cos:secret:id'] = [
    '#type' => 'textfield',
    '#title' => 'QCloud COS Secret ID',
    '#default_value' => variable_get('qcloud_cos:secret:id', ''),
    '#required' => TRUE,
  ];
  $form['required']['qcloud_cos:secret:key'] = [
    '#type' => 'textfield',
    '#title' => 'QCloud COS Secret Key',
    '#default_value' => variable_get('qcloud_cos:secret:key', ''),
    '#required' => TRUE,
  ];
  $form['required']['qcloud_cos:bucket'] = [
    '#type' => 'textfield',
    '#title' => 'QCloud COS Bucket',
    '#default_value' => variable_get('qcloud_cos:bucket', ''),
    '#required' => TRUE,
  ];
  $form['required']['qcloud_cos:region'] = [
    '#type' => 'textfield',
    '#title' => 'QCloud COS 区域',
    '#default_value' => variable_get('qcloud_cos:region', ''),
    '#required' => TRUE,
    '#description' => t('参考链接中适用于 <b>JSON API</b> 的内容:<a href="@url">@url</a>', ['@url' => 'https://www.qcloud.com/document/product/436/6224']),

  ];
  $form['required']['qcloud_cos:timeout'] = [
    '#type' => 'textfield',
    '#title' => 'QCloud COS 超时时间',
    '#default_value' => variable_get('qcloud_cos:timeout', 60),
    '#required' => TRUE,
  ];
  $form['required']['qcloud_cos:cdn'] = [
    '#default_value' => variable_get('qcloud_cos:cdn', FALSE),
    '#type' => 'checkbox',
    '#title' => '使用自定义域名',
  ];
  $form['required']['qcloud_cos:cname:domain'] = [
    '#type' => 'textfield',
    '#title' => 'CNAME Address',
    '#default_value' => variable_get('qcloud_cos:cname:domain', ''),
    '#description' => t('需要包含 http '),
  ];

  return system_settings_form($form);
}