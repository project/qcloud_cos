<?php

use QCloud\Cos\Api;
use QCloud\Cos\Auth;

/**
 * Class QCloudCOSStreamWrapper
 */
class QCloudCOSStreamWrapper implements DrupalStreamWrapperInterface {

  private $scheme = "cos";

  /**
   * @var String Instance URI
   */
  protected $uri;

  /**
   * @var q_cloud COS　connection object
   */
  protected $cos = NULL;

  /**
   * @var q_cloud COS access appId
   */
  protected $cos_app_id = '';

  /**
   * @var q_cloud COS access secretId
   */
  protected $cos_secret_id = '';

  /**
   * @var q_cloud COS access secretKey
   */
  protected $cos_secret_key = '';

  /**
   * @var q_cloud COS access bucket
   */
  protected $bucket = '';

  /**
   * @var string Domain we use to access files over http
   */
  protected $domain = NULL;

  /**
   * @var array directory listing
   */
  protected $dir = [];

  /**
   * @var array Default map for determining file mime types
   */
  protected static $mapping = NULL;

  /**
   * @var int Current read/write position
   */
  protected $position = 0;

  /**
   * @var int Total size of the object as returned by OSS (Content-length)
   */
  protected $object_size = 0;

  /**
   * @var string Object read/write buffer, typically a file
   */
  protected $buffer = NULL;

  /**
   * @var int Buffer length
   */
  protected $buffer_length = 0;

  protected $config;

  protected $region;

  protected $timeout;

  protected $cos_cdn;


  /**
   * QCloudCOSStreamWrapper constructor.
   *
   * set the COS app_id, secret_id, secret_key and bucket name
   */
  public function __construct() {
    $this->cos_app_id = variable_get('qcloud_cos:appid', '');
    $this->cos_secret_id = variable_get('qcloud_cos:secret:id', '');
    $this->cos_secret_key = variable_get('qcloud_cos:secret:key', '');
    $this->region = variable_get('qcloud_cos:region', '');
    $this->timeout = variable_get('qcloud_cos:timeout', 60);

    $this->bucket = variable_get('qcloud_cos:bucket', '');
    $this->cos_cdn = variable_get('qcloud_cos:cdn', FALSE);

    $this->domain = variable_get('qcloud_cos:cname:domain', '');


    $this->config = [
      'app_id' => $this->cos_app_id,
      'secret_id' => $this->cos_secret_id,
      'secret_key' => $this->cos_secret_key,
      'region' => $this->region,   // bucket所属地域：华北 'tj' 华东 'sh' 华南 'gz'
      'timeout' => $this->timeout,
    ];
    return $this;
  }

  /**
   * Get the COS connection object
   *
   * @return
   *   COS connection object
   */
  protected function getCOS() {
    if ($this->cos == NULL) {
      $this->cos = new Api($this->config);
    }
    return $this->cos;
  }

  /**
   * Changes permissions of the resource.
   *
   * PHP lacks this functionality and it is not part of the official stream
   * wrapper interface. This is a custom implementation for Drupal.
   *
   * @param $mode
   *   Integer value for the permissions. Consult PHP chmod() documentation
   *   for more information.
   *
   * @return
   *   Returns TRUE on success or FALSE on failure.
   */
  public function chmod($mode) {
    $this->assertConstructorCalled();
    return TRUE;
  }

  /**
   * Support for closedir().
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-closedir.php
   */
  public function dir_closedir() {
    $this->dir = [];
    return TRUE;
  }

  /**
   * Support for opendir().
   *
   * @param $uri
   *   A string containing the URI to the directory to open.
   * @param $options
   *   Unknown (parameter is not documented in PHP Manual).
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-opendir.php
   */
  public function dir_opendir($uri, $options) {
    $this->assertConstructorCalled();
    if ($uri == NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Support for readdir().
   *
   * @return
   *   The next filename, or FALSE if there are no more files in the directory.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-readdir.php
   */
  public function dir_readdir() {
    $filename = current($this->dir);
    if ($filename !== FALSE) {
      next($this->dir);
    }
    return $filename;
  }

  /**
   * Support for rewinddir().
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-rewinddir.php
   */
  public function dir_rewinddir() {
    reset($this->dir);
    return TRUE;
  }

  /**
   * Gets the name of the directory from a given path.
   *
   * This method is usually accessed through drupal_dirname(), which wraps
   * around the normal PHP dirname() function, which does not support stream
   * wrappers.
   *
   * @param $uri
   *   An optional URI.
   *
   * @return
   *   A string containing the directory name, or FALSE if not applicable.
   *
   * @see drupal_dirname()
   */
  public function dirname($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    list($scheme, $target) = explode('://', $uri, 2);
    $target = trim($target, '\/');
    $dirname = dirname($target);
    if ($dirname == '.') {
      $dirname = '';
    }
    return $scheme . '://' . $dirname;
  }

  /**
   * Returns a web accessible URL for the resource.
   *
   * This function should return a URL that can be embedded in a web page
   * and accessed from a browser. For example, the external URL of
   * "youtube://xIpLd0WQKCY" might be
   * "http://www.youtube.com/watch?v=xIpLd0WQKCY".
   *
   * @return
   *   Returns a string containing a web accessible URL for the resource.
   */
  public function getExternalUrl() {
    // TODO: Implement getExternalUrl() method.
    // Image styles support
    // Delivers the first request to an image from the private file system
    // otherwise it returns an external URL to an image that has not been
    // created yet
    $path = explode('/', $this->getLocalPath());
    if ($path[0] == 'styles') {
      if (!$this->_qcloudcos_get_object($this->uri)) {
        array_shift($path);
        return url('system/files/styles/' . implode('/', $path), ['absolute' => TRUE]);
      }
    }

    if ($this->cos_cdn) {
      return $this->domain . '/' . $this->getLocalPath();
    }

    $urls = $this->getCOS()
      ->getDownloadUrl($this->bucket, $this->getLocalPath(), 3600);

    if ($urls['code'] == 0) {
      return $urls['data']['source_url'];
    }
    return FALSE;

  }

  /**
   * @return \QCloud\Cos\Auth
   */
  protected function getAuth() {
    return new Auth($this->cos_app_id, $this->cos_secret_id, $this->cos_secret_key);
  }

  private function sign($uri, $expired = 86400) {
    $path = $this->getLocalPath($uri);

    return $this->getAuth()
      ->createReusableSignature(time() + $expired, $this->bucket, '/' . $path);
    //appSign('/' . $path, $this->bucket, );
  }

  /**
   * Returns the MIME type of the resource.
   *
   * @param $uri
   *   The URI, path, or filename.
   * @param $mapping
   *   An optional map of extensions to their mimetypes, in the form:
   *    - 'mimetypes': a list of mimetypes, keyed by an identifier,
   *    - 'extensions': the mapping itself, an associative array in which
   *      the key is the extension and the value is the mimetype identifier.
   *
   * @return
   *   Returns a string containing the MIME type of the resource.
   */
  public static function getMimeType($uri, $mapping = NULL) {
    // Load the default file map
    if (!isset(self::$mapping)) {
      include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
      self::$mapping = file_mimetype_mapping();
    }

    $extension = '';
    $file_parts = explode('.', basename($uri));

    // Remove the first part: a full filename should not match an extension.
    array_shift($file_parts);

    // Iterate over the file parts, trying to find a match.
    // For my.awesome.image.jpeg, we try:
    //   - jpeg
    //   - image.jpeg, and
    //   - awesome.image.jpeg

    while ($additional_part = array_pop($file_parts)) {
      $extension = strtolower($additional_part . ($extension ? '.' . $extension : ''));
      if (isset(self::$mapping['extensions'][$extension])) {
        return self::$mapping['mimetypes'][self::$mapping['extensions'][$extension]];
      }
    }

    return 'application/octet-stream';
  }

  /**
   * Returns the stream resource URI.
   *
   * @return
   *   Returns the current URI of the instance.
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * Support for mkdir().
   *
   * @param $uri
   *   A string containing the URI to the directory to create.
   * @param $mode
   *   Permission flags - see mkdir().
   * @param $options
   *   A bit mask of STREAM_REPORT_ERRORS and STREAM_MKDIR_RECURSIVE.
   *
   * @return
   *   TRUE if directory was successfully created.
   *
   * @see http://php.net/manual/en/streamwrapper.mkdir.php
   */
  public function mkdir($uri, $mode, $options) {
    $localpath = $this->getLocalPath($uri);
    $ret = $this->getCOS()->createFolder($this->bucket, $localpath);
    if ($ret['code'] == 0 && $ret['message'] == 'SUCCESS') {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns canonical, absolute path of the resource.
   *
   * Implementation placeholder. PHP's realpath() does not support stream
   * wrappers. We provide this as a default so that individual wrappers may
   * implement their own solutions.
   *
   * @return
   *   Returns a string with absolute pathname on success (implemented
   *   by core wrappers), or FALSE on failure or if the registered
   *   wrapper does not provide an implementation.
   */
  public function realpath() {
    return FALSE;
  }

  /**
   * Support for rename().
   *
   * If $to_uri exists, this file will be overwritten. This behavior is
   * identical to the PHP rename() function.
   *
   * @param $from_uri
   *   The uri to the file to rename.
   * @param $to_uri
   *   The new uri for file.
   *
   * @return
   *   TRUE if file was successfully renamed.
   * return true as it does not provide implementation
   *
   * @see http://php.net/manual/en/streamwrapper.rename.php
   */
  public function rename($from_uri, $to_uri) {
    $ret = $this->getCOS()->stat($this->bucket, $this->getLocalPath($from_uri));
    if ($ret['code'] == 0 && !empty($ret['data'])) {
      $this->clearBuffer();
      $this->buffer = file_get_contents($ret['data']['access_url']);
      $this->buffer_length = strlen($this->buffer);
      $this->getCOS()->delFile($this->bucket, $this->getLocalPath($from_uri));

      $temp_name = drupal_tempnam('temporary://', 'file');
      file_put_contents($temp_name, $this->buffer);
      $file = drupal_realpath($temp_name);
      $ret = $this->getCOS()
        ->upload($this->bucket, $file, $this->getLocalPath($to_uri), '', 20971520, 0);
      $this->clearBuffer();
      if ($ret['code'] == 0 && !empty($ret['data'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Support for rmdir().
   *
   * @param $uri
   *   A string containing the URI to the directory to delete.
   * @param $options
   *   A bit mask of STREAM_REPORT_ERRORS.
   *
   * @return
   *   TRUE if directory was successfully removed.
   *
   * @see http://php.net/manual/en/streamwrapper.rmdir.php
   */
  public function rmdir($uri, $options) {
    $this->assertConstructorCalled();
    $localPath = $this->getLocalPath($uri);
    $ret = $this->getCOS()->delFolder($this->bucket, $localPath);
    if ($ret['code'] == 0 && !empty($ret['data'])) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Set the absolute stream resource URI.
   *
   * This allows you to set the URI. Generally is only called by the factory
   * method.
   *
   * @param $uri
   *   A string containing the URI that should be used for this instance.
   */
  function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * Support for fclose().
   *
   * Clears the object buffer and returns TRUE
   *
   * @return
   *   TRUE
   *
   * @see http://php.net/manual/en/streamwrapper.stream-close.php
   */
  public function stream_close() {
    $this->clearBuffer();
    return TRUE;
  }

  /**
   * Support for feof().
   *
   * @return
   *   TRUE if end-of-file has been reached.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-eof.php
   */
  public function stream_eof() {
    if (!$this->uri) {
      return TRUE;
    }
    return ($this->position >= $this->object_size);
  }

  /**
   * Support for fflush(). Flush current cached stream data to storage.
   *
   * @return
   *   TRUE if data was successfully stored (or there was no data to store).
   *
   * @see http://php.net/manual/en/streamwrapper.stream-flush.php
   */
  public function stream_flush() {
    if (empty($this->buffer)) {
      return TRUE;
    }

    $matadata = $this->_qcloudcos_get_object($this->getUri());
    if ($matadata) {
      $this->clearBuffer();
      return TRUE;
    }

    $temp_name = drupal_tempnam('temporary://', 'file');
    file_put_contents($temp_name, $this->buffer);
    $file = drupal_realpath($temp_name);
    $ret = $this->getCOS()
      ->upload($this->bucket, $file, $this->getLocalPath(), '', 20971520, 0);
    $this->clearBuffer();
    if ($ret['code'] == 0 && !empty($ret['data'])) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Support for flock().
   *
   * @param $operation
   *   One of the following:
   *   - LOCK_SH to acquire a shared lock (reader).
   *   - LOCK_EX to acquire an exclusive lock (writer).
   *   - LOCK_UN to release a lock (shared or exclusive).
   *   - LOCK_NB if you don't want flock() to block while locking(not
   * supported {on} Windows).
   *
   * @return
   *   returns TRUE if lock {
   * was} successful
   *
   * @see http://php.net/manual/en/streamwrapper.stream-lock.php
   */
  public function stream_lock($operation) {
    return FALSE;
  }

  /**
   * Support for fopen(), file_get_contents(), file_put_contents() etc.
   *
   * @param $uri
   *   A string containing the URI to the file to open.
   * @param $mode
   *   The file mode ("r", "wb" etc.).
   * @param $options
   *   A bit mask of STREAM_USE_PATH and STREAM_REPORT_ERRORS.
   * @param $opened_path
   *   A string containing the path actually opened.
   *
   * @return
   *   Returns TRUE if file was opened successfully.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-open.php
   */
  public function stream_open($uri, $mode, $options, &$opened_url) {
    $this->uri = $uri;
    if (strpbrk($mode, 'wax')) {
      $this->clearBuffer();
      return TRUE;
    }
    //$ret = \Qcloud_cos\Cosapi::statFolder($this->bucket, $localPath);
    $metadata = $this->_qcloudcos_get_object($uri);

    if ($metadata) {
      $this->clearBuffer();
      $this->object_size = $metadata['filesize'];
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Support for fread(), file_get_contents() etc.
   *
   * @param $count
   *   Maximum number of bytes to be read.
   *
   * @return
   *   The string that was read, or FALSE in case of an error.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-read.php
   */
  public function stream_read($count) {
    try {
      // make sure that count doesn't exceed object size
      if ($count + $this->position > $this->object_size) {
        $count = $this->object_size - $this->position;
      }
      $data = '';
      if ($count > 0) {
        $range_end = $this->position + $count - 1;
        if ($range_end > $this->buffer_length) {
          $content = $this->_get_content($this->uri);
          $this->buffer .= $content;
          $this->buffer_length += strlen($content);
        }
        $data = substr($this->buffer, $this->position, $count);
        $this->position += strlen($data);
      }
      return $data;
    } catch (\Exception $e) {
      return NULL;
    }
  }

  /**
   * @param $uri
   *
   * @return bool|null|string
   */
  protected function _get_content($uri) {
    $localPath = $this->getLocalPath($uri);
    $temp_name = drupal_tempnam('temporary://', 'file');
    $ret = $this->getCOS()->download($this->bucket, $localPath, $temp_name);
    if ($ret['code'] == 0) {
      $content = file_get_contents($temp_name);
      return $content;
    }
    return NULL;
  }

  /**
   * Support for fseek().
   *
   * @param $offset
   *   The byte offset to got to.
   * @param $whence
   *   SEEK_SET, SEEK_CUR, or SEEK_END.
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-seek.php
   */
  public function stream_seek($offset, $whence) {
    switch ($whence) {
      case SEEK_CUR:
        // Set position to current location plus $offset
        $new_position = $this->position + $offset;
        break;
      case SEEK_END:
        // Set position to eof plus $offset
        $new_position = $this->object_size + $offset;
        break;
      case SEEK_SET:
      default:
        // Set position equal to $offset
        $new_position = $offset;
        break;
    }

    $ret = ($new_position >= 0 && $new_position <= $this->object_size);
    if ($ret) {
      $this->position = $new_position;
    }
    return $ret;
  }

  /**
   * Support for fstat().
   *
   * @return
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-stat.php
   */
  public function stream_stat() {
    return $this->_stat();
  }

  /**
   * Support for ftell().
   *
   * @return
   *   The current offset in bytes from the beginning of file.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-tell.php
   */
  public function stream_tell() {
    return $this->position;
  }

  /**
   * Support for fwrite(), file_put_contents() etc.
   *
   * @param $data
   *   The string to be written.
   *
   * @return
   *   The number of bytes written (integer).
   *
   * @see http://php.net/manual/en/streamwrapper.stream-write.php
   */
  public function stream_write($data) {
    $data_length = strlen($data);
    $this->buffer .= $data;
    $this->buffer_length += $data_length;
    $this->position += $data_length;
    return $data_length;
  }

  /**
   * Support for unlink().
   *
   * @param $uri
   *   A string containing the uri to the resource to delete.
   *
   * @return
   *   TRUE if resource was successfully deleted.
   *
   * @see http://php.net/manual/en/streamwrapper.unlink.php
   */
  public function unlink($uri) {
    $this->assertConstructorCalled();
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $localPath = $this->getLocalPath($uri);
    $ret = $this->getCOS()->delFile($this->bucket, $localPath);
    if ($ret['code'] == 0 && $ret['message'] == 'SUCCESS') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Support for stat().
   *
   * @param $uri
   *   A string containing the URI to get information about.
   * @param $flags
   *   A bit mask of STREAM_URL_STAT_LINK and STREAM_URL_STAT_QUIET.
   *
   * @return
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/en/streamwrapper.url-stat.php
   */
  public function url_stat($uri, $flags) {
    return $this->_stat($uri);
  }

  /**
   * @todo 返回相对 bucket 的路径  不包含左侧  /
   *
   * @param $uri
   */
  protected function getLocalPath($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $scheme = $this->scheme;
    $path = str_replace("{$scheme}://", '', $uri);
    $path = ltrim($path, '/');
    return $path;
  }

  /**
   * Flush the object buffers
   */
  protected function clearBuffer() {
    $this->position = 0;
    $this->object_size = 0;
    $this->buffer = NULL;
    $this->buffer_length = 0;
  }

  /**
   * Get file status
   *
   * @return
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-stat.php
   */
  protected function _stat($uri = NULL) {
    if (empty($uri)) {
      $uri = $this->getUri();
    }
    $isDir = $this->_qcloudcos_is_dir($uri);

    $ret = $this->getCOS()->stat($this->bucket, $this->getLocalPath($uri));

    $metadata = [];
    if ($ret['code'] == 0 && !empty($ret['data'])) {
      $metadata = $ret['data'];
    }

    if ($metadata || $isDir) {
      $stat = [];
      $stat[0] = $stat['dev'] = 0;
      $stat[1] = $stat['ino'] = 0;
      $stat[2] = $stat['mode'] = $isDir ? 0040000 | 0777 : 0100000;
      $stat[3] = $stat['nlink'] = 0;
      $stat[4] = $stat['uid'] = 0;
      $stat[5] = $stat['gid'] = 0;
      $stat[6] = $stat['rdev'] = 0;
      $stat[7] = $stat['size'] = 0;
      $stat[8] = $stat['atime'] = 0;
      $stat[9] = $stat['mtime'] = 0;
      $stat[10] = $stat['ctime'] = 0;
      $stat[11] = $stat['blksize'] = 0;
      $stat[12] = $stat['blocks'] = 0;
      if (!$isDir) {
        $stat[7] = $stat['size'] = isset($metadata['filesize']) ? $metadata['filesize'] : 0;
        $stat[8] = $stat['atime'] = isset($metadata['mtime']) ? $metadata['mtime'] : 0;
        $stat[9] = $stat['mtime'] = isset($metadata['mtime']) ? $metadata['mtime'] : 0;
        $stat[10] = $stat['ctime'] = isset($metadata['ctime']) ? $metadata['ctime'] : 0;
      }
      return $stat;
    }

    return FALSE;
  }

  /**
   * Determine whether the $uri is a directory
   *
   * @param $uri
   *   A string containing the uri to the resource to check. If none is given
   *   defaults to $this->uri
   *
   * @return
   *   TRUE if the resource is a directory
   */
  protected function _qcloudcos_is_dir($uri = NULL) {
    if ($uri == NULL) {
      $uri = $this->uri;
    }

    $path = $this->getLocalPath($uri);
    if (strlen($path) === 0) {
      return TRUE;
    }

    if (substr($uri, -1, 1) == '/') {
      return TRUE;
    }

    $localPath = $this->getLocalPath($uri);
    $ret = $this->getCOS()->statFolder($this->bucket, $localPath);
    if ($ret['code'] == 0 && !empty($ret['data'])) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Try to fetch an object from COS .
   *
   * @param uri
   *   A string containing the uri of the resource to check.
   *
   * @return
   *    An array if the $uri exists, otherwise FALSE.
   */
  protected function _qcloudcos_get_object($uri) {
    if ($uri == "{$this->scheme}://" || $uri == "{$this->scheme}:") {
      $metadata = $this->_qcloudcos_format_response('/', [], TRUE);
      return $metadata;
    }
    else {
      $uri = rtrim($uri, '/');
    }

    $is_dir = $this->_qcloudcos_is_dir($uri);
    $metadata = NULL;
    if ($is_dir) {
      $metadata = $this->_qcloudcos_format_response($uri, [], TRUE);
      return $metadata;
    }
    else {
      $localPath = $this->getLocalPath($uri);
      $ret = $this->getCOS()->stat($this->bucket, $localPath);
      if ($ret['code'] == 0 && !empty($ret['data'])) {
        $metadata = $this->_qcloudcos_format_response($uri, $ret['data']);
        return $metadata;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Format returned file information from COS into an array
   *
   * @param $uri
   *   A string containing the uri of the resource to check.
   * @param $response
   *   An object containing the collective metadata for the Aliyun OSS object
   * @param $is_dir
   *   A boolean indicating whether this object is a directory.
   *
   * @return
   *   An array containing formatted metadata
   */
  protected function _qcloudcos_format_response($uri, $response, $is_dir = FALSE) {
    $metadata = ['uri' => $uri];
    if (isset($response['filesize'])) {
      $metadata['filesize'] = $response['filesize'];
    }

    if (isset($response['mtime'])) {
      $metadata['mtime'] = $response['mtime'];
    }

    if (isset($response['ctime'])) {
      $metadata['ctime'] = $response['ctime'];
    }

    if ($is_dir) {
      $metadata['dir'] = 1;
      $metadata['mode'] = 0040000; // S_IFDIR indicating directory
      $metadata['mode'] |= 0777;
    }
    else {
      $metadata['dir'] = 0;
      $metadata['mode'] = 0100000; // S_IFREG indicating file
      $metadata['mode'] |= 0777; // everything is writeable
    }
    return $metadata;
  }

  /**
   * GET create dir resource path
   *
   * @param null $uri
   */
  /*protected function _qcloudcos_get_create_file_uri($uri = NULL) {
  }*/

  /**
   * Assert that the constructor has been called, call it if not.
   *
   * Due to PHP bug #40459, the constructor of this class isn't always called
   * for some of the methods. This private method calls the constructor if
   * it hasn't been called before.
   *
   * @see https://bugs.php.net/bug.php?id=40459
   */
  private function assertConstructorCalled() {
    if (empty($this->cos_app_id) && empty($this->cos_secret_key) && empty($this->cos_secret_id) && empty($this->bucket)) {
      $this->__construct();
    }
  }
}